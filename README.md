# Shopping Cart App

Made with React & Redux

## Readme

To run this app, clone this repository on your machine.

Go to the root folder of the project and follow these steps to start the app:

- 'yarn install' or 'npm install'
- 'yarn run build' or 'npm run build'

You will need something to serve builded app. You can use 'serve' package:

- 'yarn global add serve' or 'npm install -g serve'
- 'serve -s build'

Already working app can be found on Heroku: https://ninja-cart.herokuapp.com/   
 

## PWA Capabilities

- Application can be installed on homescreen of mobile device by visiting https://ninja-cart.herokuapp.com/ few times on mobile device.
- Application can be viewed with no internet connection. 
- Assets are cached. 

## TypeScript version

There is a TypeScript version of the app. 
You can find it on: https://bitbucket.org/endeauvirr/ts-shopping-cart/