import Dinero from 'dinero.js/build/esm/dinero.min.js';

export const validateQuantity = (newValue, previousValue) => {
  const convertedValue = Number(newValue);
  
  if (isNaN(convertedValue) || convertedValue < 1 || convertedValue > 100) {
    return previousValue
  }
  
  return convertedValue;
};

export const calculateCartValue = (products) => {
  let sum = 0;
  
  for (let product of products) {
    sum += (product.quantity * product.price) * 100
  }
  
  return Dinero({ amount: sum, currency: 'EUR' }).toFormat();
};

export const formatPrice = (rawPrice) => {
  const parsedPrice = parseFloat(rawPrice) * 100;
  
  return Dinero({ amount: parsedPrice, currency: 'EUR' }).toFormat();
};

export const formatShippingPrice = (price) => {
  return (price === 0 ? 'Free' : formatPrice(price));
};