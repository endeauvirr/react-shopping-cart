import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Header.scss';
import logo from '../../assets/images/logo.png';
import logoRetina from '../../assets/images/logo@retina.png';

const Header = () => {
  return (
    <header className={styles.headerWrapper}>
      <Link to={'/'} className={styles.logoWrapper}>
        <picture>
          <img srcSet={`${logo}, ${logoRetina} 2x`} className={styles.logo} alt="NinjaCart" />
        </picture>
        <p className={styles.logoText}>
          NinjaCart
        </p>
      </Link>
    </header>
  );
};

export default Header;