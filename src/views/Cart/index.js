import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSadTear } from '@fortawesome/free-solid-svg-icons';

import Layout from '../Layout/';
import Products from '../../components/Products/';
import ProductSkeleton from '../../components/Products/ProductSkeleton/';
import CartSummary from '../../components/CartSummary/';
import { CartType } from '../../types/Cart';
import { pendingFetchProducts, updateCartValue } from '../../actions/cart';
import { setInitialShippingProvider } from '../../actions/shippingProviders';
import { updateCartProgress } from '../../actions/cartProgress';
import { routes } from '../../utils/constants';

import styles from './Cart.scss';

class Cart extends Component {
  state = {
    loading: true
  }
  handleProceedToNextStep = () => {
    const { history, updateCartProgress, updateCartValue, cart, setInitialShippingProvider } = this.props;
  
    updateCartValue(cart.products);
    setInitialShippingProvider(cart.products);

    updateCartProgress({
      cartConfirmed: true,
      shippingConfirmed: false
    });
    
    history.push(routes.shippingUrl);
  }
  componentDidUpdate(prevProps) {
    const { products } = this.props.cart;
    
    if (prevProps.cart.products.length !== products.length) {
      this.setState({
        loading: false
      })
    }
  }
  componentDidMount() {
    const { pendingFetchProducts } = this.props;
    const { areProductsFetched, products } = this.props.cart;

    if (!areProductsFetched) {
      pendingFetchProducts();
    }
    
    if (products.length !== 0) {
      this.setState({ loading: false })
    }
  }
  render() {
    const { loading } = this.state;
    const { products } = this.props.cart;
    
    return (
      <Layout stepNumber={1} title={'Product list'} >
        {
          loading ? (
      
            <ProductSkeleton />
    
          ) : (
            <Fragment>
              {
                products.length === 0 ? (
                  <p className={styles.emptyCart}>
                    Your cart is empty <FontAwesomeIcon icon={faSadTear} size="lg"/>
                  </p>
                ) : (
                  <Fragment>
                    <Products products={products}/>
                    <CartSummary products={products} />
                  </Fragment>
                )
              }

              <footer className={styles.footerWrapper}>
                {
                  products.length !== 0 &&
                  <button
                    type="button"
                    onClick={this.handleProceedToNextStep}
                    className={styles.proceedButton}
                  >
                    Buy
                  </button>
                }
              </footer>
            </Fragment>
          )
        }
        
      </Layout>
    )
  }
}

Cart.propTypes = {
  cart: CartType.isRequired,
  updateCartProgress: PropTypes.func.isRequired,
  pendingFetchProducts: PropTypes.func.isRequired,
  setInitialShippingProvider: PropTypes.func.isRequired
};


const mapStateToProps = ({ cart }) => ({ cart });

const mapDispatchToProps = {
  pendingFetchProducts,
  updateCartValue,
  updateCartProgress,
  setInitialShippingProvider
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);