import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import Layout from '../Layout/';
import { CartType } from "../../types/Cart";
import { ShippingProviderType } from '../../types/ShippingProvider';
import { PaymentProviderType } from '../../types/PaymentProvider';
import { CartProgressType } from '../../types/CartProgress';
import { formatPrice } from '../../utils/helpers';
import { routes } from '../../utils/constants';
import { setPaymentProvider } from '../../actions/paymentProviders';
import {
  getProductsQuantity,
  getShippingProviderData,
  getSelectedPaymentProvider
} from '../../selectors/payment-view';

import styles from './Payment.scss';

class Payment extends Component {
  state = {
    agreementsChecked: false
  }
  setBuyerAgreement = () => {
    this.setState((prevState) => ({
      agreementsChecked: !prevState.agreementsChecked
    }))
  }
  setPaymentProvider = (event) => {
    const { setPaymentProvider } = this.props;
  
    setPaymentProvider(Number(event.currentTarget.value))
  }
  render() {
    const {
      paymentProviders,
      cartProgress,
      productsQuantity,
      selectedShippingProvider,
      selectedPaymentProviderId
    } = this.props;
    
    const { cartConfirmed, shippingConfirmed } = cartProgress;
    
    if ( !cartConfirmed || !shippingConfirmed ) {
      return (
        <Redirect to={routes.cartUrl} />
      )
    }

    const { agreementsChecked } = this.state;
    const { cartValue, freeShippingAvailable, buyer } = this.props.cart;

    const summaryCartValue = (freeShippingAvailable ? cartValue : cartValue + selectedShippingProvider.value);
    
    const formattedCartValue = formatPrice(summaryCartValue);
 
    return (
      <Layout stepNumber={3} title={'Payment'} >
        <div className={styles.paymentWrapper}>
          <h2>Summary</h2>
          <p>
            You will buy <strong>{productsQuantity}</strong> {(productsQuantity === 1) ? 'product': 'products'} for { formattedCartValue }&nbsp;
            delivered by <strong>{selectedShippingProvider.name}</strong> { freeShippingAvailable ? '(free delivery)' : '(delivery costs included)'}.
          </p>
    
          <hr/>
    
          <h3>Address</h3>
          <p>
            {buyer.name}
            <br />{buyer.address}
            {
              !!buyer.phone &&
              <Fragment>
                <br />+48 {buyer.phone}
              </Fragment>
            }
            <br />{buyer.email}
          </p>
    
    
          <div className={styles.paymentProviders}>
            <label htmlFor="paymentProviders">Payment method: </label>
      
            <div className={styles.selectControl}>
              <select name="paymentProviders" defaultValue={selectedPaymentProviderId} onChange={this.setPaymentProvider}>
                {
                  paymentProviders.map(provider => {
                    return (
                      !provider.disabled &&
                      <option
                        key={provider.id}
                        value={provider.id}
                      >
                        {provider.name}
                      </option>
                    )
                  })
                }
              </select>
              <div className={styles.selectControlArrow}></div>
            </div>
    
          </div>
    
    
          <div className={styles.buyerAgreement}>
            <input type="checkbox" id="buyerAgreement" name="buyerAgreement" onChange={this.setBuyerAgreement} />
            <label htmlFor="buyerAgreement">I read and accept regulations. * (mandatory agreement)</label>
          </div>
    
    
          <footer className={styles.paymentFooter}>
            <Link to={routes.shippingUrl}>back to shipping</Link>
            <button
              type="button"
              disabled={!agreementsChecked}
              className={styles.proceedButton}
            >
              Pay
            </button>
          </footer>
  
        </div>
      </Layout>
    )
  }
}

Payment.propTypes = {
  cart: CartType.isRequired,
  cartProgress: CartProgressType.isRequired,
  shippingProviders: PropTypes.arrayOf(ShippingProviderType),
  paymentProviders: PropTypes.arrayOf(PaymentProviderType),
  setPaymentProvider: PropTypes.func,
  selectedShippingProvider: ShippingProviderType.isRequired,
  selectedPaymentProviderId: PropTypes.number.isRequired,
  productsQuantity: PropTypes.number.isRequired
};

const mapStateToProps = ({ cart, shippingProviders, paymentProviders, cartProgress }) => {
  return {
    cart,
    shippingProviders,
    paymentProviders,
    cartProgress,
    selectedShippingProvider: getShippingProviderData(cart.buyer, shippingProviders),
    selectedPaymentProviderId: getSelectedPaymentProvider(paymentProviders),
    productsQuantity: getProductsQuantity(cart.products),
  }
};

const mapDispatchToProps = {
  setPaymentProvider
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);