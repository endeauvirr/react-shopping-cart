import React from 'react';
import PropTypes from 'prop-types';

import styles from './Layout.scss';

const Layout = (props) => {
  const { stepNumber, title } = props;
  return (
    <section className={styles.container}>
      <header className={styles.headerWrapper}>
        <h3>
          <span className={styles.label}>step.{stepNumber}</span> {title}
        </h3>
      </header>
      
      {props.children}
      
    </section>
  );
};

Layout.propTypes = {
  stepNumber: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired
};

export default Layout;