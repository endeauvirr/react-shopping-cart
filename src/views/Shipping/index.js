import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Layout from '../Layout/';
import ShippingForm from '../../components/ShippingForm/';
import { CartType } from '../../types/Cart';
import { CartProgressType } from '../../types/CartProgress';
import { ShippingProviderType } from '../../types/ShippingProvider';
import { updateBuyerData } from '../../actions/cart';
import { updateCartProgress } from '../../actions/cartProgress';
import { routes } from '../../utils/constants';

class Shipping extends Component {
  handleProceedToNextStep = () => {
    const { updateCartProgress, history } = this.props;
    
    updateCartProgress({
      cartConfirmed: true,
      shippingConfirmed: true
    });
    
    history.push(routes.paymentUrl);
  }
  render() {
    const { shippingProviders, cart, updateBuyerData, history, cartProgress } = this.props;
    
    const { cartConfirmed } = cartProgress;
    
    if (!cartConfirmed) {
      return (
        <Redirect to={routes.cartUrl} />
      )
    }
    
    return (
      <Layout stepNumber={2} title={'Shipping'} >
        <ShippingForm
          shippingProviders={shippingProviders}
          freeShippingAvailable={cart.freeShippingAvailable}
          updateBuyerData={updateBuyerData}
          handleProceedToNextStep={this.handleProceedToNextStep}
          history={history}
          buyer={cart.buyer}
        />
      </Layout>
    )
  }
}

const mapStateToProps = ({ cart, shippingProviders, cartProgress }) => {
  return {
    cart,
    cartProgress,
    shippingProviders
  }
};

const mapDispatchToProps = {
  updateBuyerData,
  updateCartProgress
};


Shipping.propTypes = {
  cart: CartType.isRequired,
  cartProgress: CartProgressType.isRequired,
  shippingProviders: PropTypes.arrayOf(ShippingProviderType),
  updateBuyerData: PropTypes.func.isRequired,
  updateCartProgress: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Shipping);