import PropTypes from 'prop-types';
import { ProductType } from "./Product";
import { BuyerType} from "./Buyer";

export const CartType = PropTypes.shape({
  products: PropTypes.arrayOf(ProductType),
  buyer: BuyerType,
  cartValue: PropTypes.number.isRequired,
  freeShippingAvailable: PropTypes.bool.isRequired,
  areProductsFetched: PropTypes.bool.isRequired
});
