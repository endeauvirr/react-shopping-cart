import PropTypes from 'prop-types';

export const ShippingProviderType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  selected: PropTypes.bool.isRequired,
  disabled: PropTypes.bool
});
