import PropTypes from 'prop-types';

export const PaymentProviderType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  selected: PropTypes.bool
});
