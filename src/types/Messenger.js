import PropTypes from 'prop-types';
import { MessageType } from "./Message";

export const MessengerType = PropTypes.shape({
  messages: PropTypes.arrayOf(MessageType)
});