import PropTypes from 'prop-types';

export const BuyerType = PropTypes.shape({
  name: PropTypes.string,
  address: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  shippingProviderId: PropTypes.string
})