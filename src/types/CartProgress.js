import PropTypes from 'prop-types';

export const CartProgressType = PropTypes.shape({
  cartConfirmed: PropTypes.bool.isRequired,
  shippingConfirmed: PropTypes.bool.isRequired
});
