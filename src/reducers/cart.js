import constants from '../utils/constants';
import { validateQuantity } from "../utils/helpers";

const {
  FETCH_PRODUCTS,
  INCREASE_QUANTITY,
  DECREASE_QUANTITY,
  CHANGE_QUANTITY,
  REMOVE_FROM_CART,
  UPDATE_CART_VALUE,
  UPDATE_BUYER_DATA
} = constants;

const initialState = {
  products: [],
  buyer: {},
  cartValue: 0,
  freeShippingAvailable: false,
  areProductsFetched: false
};

const cart = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_PRODUCTS : {
      const { products } = action;

      if (typeof products !== 'object') {
        return state;
      }

      return {
        ...state,
        products: state.products.concat(action.products),
        areProductsFetched: true
      }
    }
    case INCREASE_QUANTITY : {
      const { id } = action.product;
      
      return {
        ...state,
        products: state.products.map(product => {
          if (product.id === id) {
            product.quantity++;
            return product
          }
          
          return product
        })
      }
    }
    case DECREASE_QUANTITY : {
      const { id } = action.product;
  
      return {
        ...state,
        products: state.products.map(product => {
          if (product.id === id) {
            
            product.quantity--;
            return product
          }
      
          return product
        })
      }
    }
    case CHANGE_QUANTITY : {
      const { id, quantity } = action.product;
      
      return {
        ...state,
        products: state.products.map(product => {
          if (product.id === id) {
            product.quantity = validateQuantity(quantity, product.quantity);
            return product
          }
      
          return product
        })
      }
    }
    case REMOVE_FROM_CART : {
      const { id } = action.product;
      const { products } = state;
      
      if (products.length === 1) {
        return {
          ...state,
          areProductsFetched: false,
          products: state.products.filter(product => {
            return (product.id !== id)
          })
        }
      }
      
      return {
        ...state,
        products: state.products.filter(product => {
          return (product.id !== id)
        })
      }
    }
    case UPDATE_CART_VALUE : {
      const cartValue = action.products.reduce((prevValue, current) => {
        return prevValue + (parseInt(current.price, 0) * current.quantity)
      }, 0);

      return {
        ...state,
        cartValue,
        freeShippingAvailable: (cartValue > 200)
      }
    }
    case UPDATE_BUYER_DATA : {
      const { buyer } = action;
      
      return {
        ...state,
        buyer
      }
    }
    default : {
      return state;
    }
  }
};

export default cart;