import constants from '../utils/constants';

const {
  SET_INITIAL_SHIPPING_PROVIDER
} = constants;

const initialState = [
  {
    id: 1,
    name: 'ninjPost',
    value: 0,
    selected: false,
    disabled: false
  },
  {
    id: 2,
    name: 'D7L',
    value: 15.99,
    selected: false,
    disabled: false
  },
  {
    id: 3,
    name: '7post',
    value: 7.99,
    selected: false,
    disabled: false
  }
];

const shippingProviders = (state = initialState, action) => {
  switch(action.type) {
    case SET_INITIAL_SHIPPING_PROVIDER : {
      const { products } = action;
      
      const isNinjPostDisabled = products.length > 3;

      return state.map(provider => {
        
        if (provider.id === 1) {
          return {
            ...provider,
            disabled: isNinjPostDisabled
          }
        }
        
        return {
          ...provider
        }
      });
    }
    default : {
      return state;
    }
  }
};

export default shippingProviders;