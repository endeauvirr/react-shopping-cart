import constants from '../utils/constants';

const {
  UPDATE_CART_PROGRESS
} = constants;

const initialState = {
  cartConfirmed: false,
  shippingConfirmed: false
};

const shoppingProgress = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_CART_PROGRESS : {
      return {
        ...state,
        ...action.status
      }
    }
    default : {
      return state;
    }
  }
};

export default shoppingProgress;