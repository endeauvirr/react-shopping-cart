import constants from '../utils/constants';

const { SET_PAYMENT_PROVIDER } = constants;

const initialState = [
  {
    id: 1,
    name: 'Pay7',
    selected: true
  },
  {
    id: 2,
    name: 'Cash payment'
  }
];

const paymentProviders = (state = initialState, action) => {
  switch(action.type) {
    case SET_PAYMENT_PROVIDER : {
      const { id } = action;
      
      return state.map(provider => {
        if (provider.id === id) {
          provider.selected = true;
          return provider
        }
        
        provider.selected = false;
        return provider;
      })
      
    }
    default : {
      return state;
    }
  }
};


export default paymentProviders;