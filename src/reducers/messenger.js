import constants from '../utils/constants';

const {
  PUT_MESSAGE,
  REMOVE_MESSAGE
} = constants;


const initialState = {
  messages: []
}

export const messenger = (state = initialState, action) => {
  switch(action.type) {
    case PUT_MESSAGE : {
      const { message } = action;
      const { messages } = state;

      return {
        ...state,
        messages: [...messages, message]
      }
    }
    case REMOVE_MESSAGE : {
      const { id } = action;
      const { messages } = state;

      const updatedMessages = messages.filter(currentMessage => currentMessage.id !== id);
      
      return {
        ...state,
        messages: updatedMessages
      }
    }
    default : {
      return state;
    }
  }
};

export default messenger;