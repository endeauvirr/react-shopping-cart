import { combineReducers } from 'redux';

import cart from '../reducers/cart';
import cartProgress from '../reducers/cartProgress';
import shippingProviders from '../reducers/shippingProviders';
import paymentProviders from '../reducers/paymentProviders';
import messenger from '../reducers/messenger';

export default combineReducers({
  cart,
  cartProgress,
  shippingProviders,
  paymentProviders,
  messenger
});
