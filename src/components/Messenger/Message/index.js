import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { MessageType } from '../../../types/Message';

import AppStyles from '../../App/App.scss';
import styles from './Message.scss';

import className from 'classnames/bind';

const cx = className.bind({...AppStyles, ...styles});

class Message extends Component {
  state = {
    timeout: 3000
  }
  componentDidMount() {
    const { timeout } = this.state;
    const { removeMessage, message } = this.props;
    const { id } = message;
    
    const timeoutId = setTimeout(() => removeMessage(id), timeout);
    
    this.setState({ timeoutId });
  }
  componentWillUnmount() {
    const { timeoutId } = this.state;

    clearTimeout(timeoutId);
  }
  render() {
    const { removeMessage } = this.props;
    const { id, type, message } = this.props.message;
    
    return (
      <div className={cx('messageHolder', `type-${type}`)}>
        <div className={cx('container', 'messageContainer')}>
          <p className={styles.message}>
            {message}
          </p>
      
          <button
            type="button"
            className={styles.removeButton}
            onClick={() => removeMessage(id)}
          >
            <FontAwesomeIcon icon={faTimes} size='lg' />
          </button>
        </div>
      </div>
    )
  }
}

Message.propTypes = {
  message: MessageType,
  removeMessage: PropTypes.func.isRequired
};

export default Message;