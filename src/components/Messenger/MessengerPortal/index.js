import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import Message from '../Message/';
import { MessageType } from '../../../types/Message';

import styles from './MessengerPortal.scss';

const messengerRoot = document.getElementById('messenger-root');

const MessengerPortal = ({ messages, removeMessage }) => {
  return ReactDOM.createPortal(
    <div className={styles.messengerWrapper}>
      {
        messages.map(message => (
          <Message message={message} key={message.id} removeMessage={removeMessage} />
        ))
      }
    </div>
    , messengerRoot)
};

MessengerPortal.propTypes = {
  messages: PropTypes.arrayOf(MessageType),
  removeMessage: PropTypes.func.isRequired
};

export default MessengerPortal;