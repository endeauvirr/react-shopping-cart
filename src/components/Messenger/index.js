import React from 'react';
import PropTypes from 'prop-types';

import MessengerPortal from './MessengerPortal/';
import { MessageType } from '../../types/Message';

const Messenger = (props) => {
  const { messages, removeMessage } = props;
  
  return <MessengerPortal messages={messages} removeMessage={removeMessage} />
};

Messenger.propTypes = {
  messages: PropTypes.arrayOf(MessageType),
  removeMessage: PropTypes.func.isRequired
};

export default Messenger;