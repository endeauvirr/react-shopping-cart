import React, {Fragment} from 'react';
import PropTypes from 'prop-types';

import { calculateCartValue } from '../../utils/helpers';
import { ProductType } from '../../types/Product';

import styles from './CartSummary.scss';

const CartSummary = (props) => {
  const { products } = props;

  return (
    <Fragment>
      <p className={styles.summary}>
        <span className={styles.label}>Grand total</span>
        { calculateCartValue(products) }
        </p>
    </Fragment>
  );
};

CartSummary.propTypes = {
  products: PropTypes.arrayOf(ProductType)
};

export default CartSummary;