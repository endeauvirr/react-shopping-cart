import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import AppRouter from '../../routes';
import Messenger from '../Messenger/';
import { MessengerType } from '../../types/Messenger';
import { removeMessage } from '../../actions/messenger';


import styles from './App.scss';

const App = (props) => {
  const { messages } = props.messenger;
  const messengerActive = (messages.length > 0);
  
  return (
    <Fragment>
      {
        messengerActive && <Messenger messages={messages} removeMessage={props.removeMessage} />
      }
    
      <div className={styles.container}>
        <AppRouter />
      </div>
    </Fragment>
  )
};

App.propTypes = {
  messenger: MessengerType.isRequired
};

const mapStateToProps = ({ messenger }) => ({ messenger });

const mapDispatchToProps = {
  removeMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
