import React, {Component} from 'react';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import {
  increaseQuantity,
  decreaseQuantity,
  changeQuantity,
  handleRemoveFromCart
} from '../../../actions/cart';
import { formatPrice } from '../../../utils/helpers';

import styles from './Product.scss';

const cx = classNames.bind(styles);

class Product extends Component {
  changeQuantity = (event) => {
    const { id, changeQuantity } = this.props;
  
    changeQuantity(id, event.currentTarget.value);
  }
  increaseQuantity = () => {
    const { id, increaseQuantity } = this.props;

    increaseQuantity(id);
  }
  decreaseQuantity = () => {
    const { id, decreaseQuantity } = this.props;
  
    decreaseQuantity(id);
  }
  handleRemoveFromCart = () => {
    const { id, handleRemoveFromCart } = this.props;
    
    handleRemoveFromCart(id);
  }
  render() {
    const { name, image, price, description, quantity } = this.props;
    
    return (
      <div className={styles.productContainer}>
        
        <div className={styles.productNameWrapper}>
          
          <div className={styles.imageWrapper}>
            <img src={image} alt={name} />
          </div>
          <div className={styles.nameWrapper}>
            <h3>{name}</h3>
            <p>{description}</p>
          </div>
          
        </div>
  
        <div className={styles.quantityWrapper}>
          <button
            type="button"
            onClick={this.decreaseQuantity}
            className={styles.quantityButton}
            disabled={(quantity === 1)}
          >
            -
          </button>
          <input
            type="text"
            name="quantity"
            value={quantity}
            onChange={this.changeQuantity}
            onFocus={(event) => event.currentTarget.select()}
            className={cx('quantityInput')}
            maxLength='3'
          />
          <button
            type="button"
            onClick={this.increaseQuantity}
            className={styles.quantityButton}
            disabled={(quantity >= 100)}
          >
            +
          </button>
        </div>
  
        <div className={styles.priceWrapper}>
          <p className={styles.price}>
            <span className={styles.label}>Unit price:</span>
            {formatPrice(price)}
          </p>
          <p className={cx('price', 'summary')}>
            <span className={styles.label}>Summary:</span>
            {formatPrice(price * quantity)}
          </p>
        </div>
  
        <button
          type="button"
          className={styles.removeProduct}
          onClick={this.handleRemoveFromCart}
        >
          <FontAwesomeIcon icon={faTrashAlt} size="lg" />
        </button>
      </div>
    )
  }
}

Product.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  quantity: PropTypes.number.isRequired,
  image: PropTypes.string,
  description: PropTypes.string,
  increaseQuantityAction: PropTypes.func,
  decreaseQuantityAction: PropTypes.func,
  changeQuantityAction: PropTypes.func,
  handleRemoveFromCartAction: PropTypes.func
};

const mapDispatchToProps = {
  increaseQuantity,
  decreaseQuantity,
  changeQuantity,
  handleRemoveFromCart
};

export default connect(null, mapDispatchToProps)(Product);