import React, {Fragment} from 'react';
import PropTypes from 'prop-types';

import { ProductType } from '../../types/Product';
import Product from './Product/';

const Products = (props) => {
  const { products } = props;
  
  return (
    <Fragment>
      {
        products.map(({id, ...product }) => {
          return (
            <Product key={id} id={id} {...product} />
          )
        })
      }
    </Fragment>
  );
};

Products.propTypes = {
  products: PropTypes.arrayOf(ProductType)
};

export default Products;