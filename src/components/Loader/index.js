import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';

import styles from './Loader.scss';

const Loader = () => {
  return (
    <div className={styles.loaderWrapper}>
      <FontAwesomeIcon icon={faCircleNotch} spin size="3x" />
    </div>
  );
};

export default Loader;