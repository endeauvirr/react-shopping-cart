import React, { Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import createAsyncComponent from '../components/AsyncComponent';
import Header from '../views/Header/';
import { routes } from '../utils/constants';

const AsyncCart = createAsyncComponent(() => import('../views/Cart/'));
const AsyncShipping = createAsyncComponent(() => import('../views/Shipping/'));
const AsyncPayment = createAsyncComponent(() => import('../views/Payment/'));

const AppRouter = () => {
  return (
    <Router>
      <Fragment>
        <Header />

        <Switch>
          <Route path='/' exact render={() => (<Redirect to={routes.cartUrl} />)} />
          <Route path={routes.cartUrl} exact component={AsyncCart} />
          <Route path={routes.shippingUrl} exact component={AsyncShipping} />
          <Route path={routes.paymentUrl} exact component={AsyncPayment} />
          <Route render={() => (<Redirect to={routes.cartUrl} />)} />
        </Switch>
      </Fragment>
    </Router>
  )
};

export default AppRouter;