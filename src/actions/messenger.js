import constants from '../utils/constants';

const {
  PUT_MESSAGE,
  REMOVE_MESSAGE
} = constants;


export const putMessage = (message) => ({
  type: PUT_MESSAGE,
  message
});


export const removeMessage = (id) => ({
  type: REMOVE_MESSAGE,
  id
});