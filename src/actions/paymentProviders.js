import constants from '../utils/constants';

const {
  SET_PAYMENT_PROVIDER
} = constants;

export const setPaymentProvider = (id) => ({
  type: SET_PAYMENT_PROVIDER,
  id
});