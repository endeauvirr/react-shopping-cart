import { v4 as uuid } from 'uuid';

import api from '../utils/api';
import constants from '../utils/constants';
import { putMessage } from './messenger';

const {
  FETCH_PRODUCTS,
  INCREASE_QUANTITY,
  DECREASE_QUANTITY,
  CHANGE_QUANTITY,
  REMOVE_FROM_CART,
  UPDATE_CART_VALUE,
  UPDATE_BUYER_DATA
} = constants;

export const fetchProducts = (products) => ({
  type: FETCH_PRODUCTS,
  products
});

export const pendingFetchProducts = () => {
  return (dispatch) => {
    fetch(api.fetchProductsURL)
      .then(response => {
        const { status } = response;
        
        if (status === 200 || status === 201) {
          return response.json()
        }
  
        dispatch(putMessage({
          id: uuid(),
          message: 'We could not get your purchases. Please try refresh the page!',
          type: 'error'
        }));
  
        return [];
      })
      .then(products => {
        dispatch(fetchProducts(products))
      })
      .catch(error => {
        throw new Error(`Error while fetching products: ${error}`);
      })
  }
};

export const increaseQuantity = (id) => ({
  type: INCREASE_QUANTITY,
  product: {
    id
  }
});

export const decreaseQuantity = (id) => ({
  type: DECREASE_QUANTITY,
  product: {
    id
  }
});


export const changeQuantity = (id, quantity) => ({
  type: CHANGE_QUANTITY,
  product: {
    id,
    quantity
  }
});

export const removeFromCart = (id) => ({
  type: REMOVE_FROM_CART,
  product: {
    id
  }
});

export const handleRemoveFromCart = (id) => {
  return (dispatch) => {
    dispatch(removeFromCart(id));
    dispatch(putMessage({
      id: uuid(),
      message: 'Product was removed.',
      type: 'info'
    }))
  }
}

export const updateCartValue = (products) => ({
  type: UPDATE_CART_VALUE,
  products
});

export const updateBuyerData = (buyer) => ({
  type: UPDATE_BUYER_DATA,
  buyer
});