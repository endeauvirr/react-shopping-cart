import constants from '../utils/constants';

const {
  SET_INITIAL_SHIPPING_PROVIDER
} = constants;

export const setInitialShippingProvider = (products) => {
  return {
    type: SET_INITIAL_SHIPPING_PROVIDER,
    products
  }
};