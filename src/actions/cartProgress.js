import constants from '../utils/constants';

const {
  UPDATE_CART_PROGRESS
} = constants;

export const updateCartProgress = (status) => ({
  type: UPDATE_CART_PROGRESS,
  status
})