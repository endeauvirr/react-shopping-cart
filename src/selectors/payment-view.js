export const getProductsQuantity = (products) => {
  return products.reduce((prev, current) => prev + current.quantity, 0);
};

export const getShippingProviderData = (buyer, shippingProviders) => {
  return shippingProviders.filter(provider => {
    return provider.id === Number(buyer.shippingProviderId)
  }).shift();
};

export const getSelectedPaymentProvider = (paymentProviders) => {
  const selectedProvider = paymentProviders.filter(provider => provider.selected);
  
  if (selectedProvider.length === 0) {
    return 1
  }
  
  return selectedProvider.shift().id;
};
